This project is inactive.

This project uses this Discord bot to create a game for the stock market.

First time start:  
An options file and an admins file get created in the project root folder. In these you can specify your Discord admins and further options. 



Optionsfile:  
-Bot token for Discord (you will need to create your own one if you want to copy this bot)  
-Alphavantage API key (you will need to get your own API key if you want to copy this bot)  
-Discord Client and secret are deprecated and don't need to be filled.  
  
Done:  
-Code boneworks  
-Able to get prices from stocks  
-Buy and sell stocks according to their current prices in real life.
  
  
Missing features:  
-Currency features  

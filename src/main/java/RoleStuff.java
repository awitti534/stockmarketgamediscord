import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class RoleStuff {

    static final File adminFile = new File("admins.txt");
    static Set<String> admins = new HashSet<>();

    public static void initialize() throws IOException {
        if (adminFile.exists()){
            //Read contents

            FileReader fr = new FileReader(adminFile);
            BufferedReader br = new BufferedReader(fr);

            String input = br.readLine();
            System.out.println("Input: " + input);

            //Read until no more content
            while (input != null){

                //Ignore comments
                if (input.charAt(0) != '!'){

                    //Ignore lines with whitespace
                    if(!input.contains(" ")){
                        admins.add(input);
                    }
                }
                input = br.readLine();
            }

            //Debug for amount of admins.
            if (amountAdmins() == 1)
                System.out.println("Currently we have 1 admin active.");
            else
                System.out.println("Currently we have " + amountAdmins() + "admins active.");

            br.close();

        } else {
            //Create file
            boolean success = adminFile.createNewFile();

            if (success){
                System.out.println("Created admin file.");
            } else {
                System.out.println("Error creating admin file.");
            }

            //Write comment for the beginning of the file.
            FileWriter fw = new FileWriter(adminFile);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("!Comments start with !. Lines with whitespace get ignored. One Discord ID per line.");
            bw.close();
        }
    }

    public static Set<String> getAdminSet(){
        return admins;
    }

    public static int amountAdmins(){
        return admins.size();
    }

}

import java.sql.*;

public class Bank {

    private static final Bank bank = new Bank();
    private static Connection connection;
    private static final String DB_Path = "db.db";
    private static Options options;

    private Bank() {
    }

    public static void setOptions(Options options) {
        Bank.options = options;
    }

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e){
            System.err.println("Error loading JDBC-Driver");
            e.printStackTrace();
        }
    }

    void initDBConnection() {
        try {
            if (connection != null)
                return;
            System.out.println("Creating connection to DB");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_Path);
            if (!connection.isClosed())
                System.out.println("Connection to DB established");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    if (!connection.isClosed() && connection != null){
                        connection.close();
                        if (connection.isClosed())
                            System.out.println("Connection to DB closed");
                    }
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
        });
    }

    static Bank getInstance(){
        return bank;
    }

    public static Connection getConnection(){
        return connection;
    }


    /**
     * Use this function the first time you want to use the program. It clears the
     * current database tables and creates new ones.
     * @throws SQLException Throws an SQL Exception if something goes wrong.
     */
    void firstTimeSetup () throws SQLException {
        createTableCurrencies();
        createTableInvestors();
        createTableStocks();
        createTableInventory();
        System.out.println("Completed setting up the database.");
    }

    /**
     * This private function creates the table for currencies. Fields are:
     * name for the name of the currency.
     * value for the price of the currency.
     * @throws SQLException Throws an SQL Exception if something goes wrong.
     */
    private void createTableCurrencies() throws SQLException {
        Statement statement = connection.createStatement();

        //Create Table currencies
        statement.executeUpdate("DROP TABLE IF EXISTS currencies;");
        statement.executeUpdate("CREATE TABLE currencies (" +
                "  currency CHAR(3) NOT NULL," +
                "  `value` FLOAT," +
                "  PRIMARY KEY (currency) " +
                "); ");
        statement.close();
    }

    private void createTableInvestors() throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DROP TABLE IF EXISTS investors;");
        statement.executeUpdate("CREATE TABLE investors (" +
                " name LONG PRIMARY KEY," +
                " currency CHAR(3)," +
                " funds FLOAT" +
                "); ");
        statement.close();
    }

    private void createTableStocks() throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DROP TABLE IF EXISTS stocks;");
        statement.executeUpdate("CREATE TABLE stocks (" +
                " ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " name varchar(100) UNIQUE NOT NULL, " +
                " `value` FLOAT );" );
        statement.close();
    }

    private void createTableInventory() throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DROP TABLE IF EXISTS inventory;");
        statement.executeUpdate("CREATE TABLE inventory (" +
                " stockname VARCHAR(100) NOT NULL, " +
                " investor LONG NOT NULL, " +
                " amount INT NOT NULL, " +
                " PRIMARY KEY (stockname, investor) ) ;");
        statement.close();
    }

    public String updateStocks(String stock){
        StringBuilder sb = new StringBuilder();
        String[] args = new String[1];
        args[0] = stock;

        //Update data of the stock in a new Thread
        HTTPConnection httpConnection = new HTTPConnection(options, "getStock", args);
        Thread thread = new Thread(httpConnection);
        thread.start();

        sb.append("Trying to update stock ").append(stock).append("now.");

        return sb.toString();
    }
    public String updateStockSynced(String stock){
        StringBuilder sb = new StringBuilder();
        String[] args = new String[1];
        args[0] = stock;

        //Update data of the stock in a new Thread
        HTTPConnection httpConnection = new HTTPConnection(options, "getStock", args);
        Thread thread = new Thread(httpConnection);
        thread.run();

        sb.append("Trying to update stock ").append(stock).append("now.");

        return sb.toString();
    }

    public String updateStockNow(String stock){
        StringBuilder sb = new StringBuilder();
        String[] args = new String[1];
        args[0] = stock;

        //Update data of the stock in a new Thread
        HTTPConnection httpConnection = new HTTPConnection(options, "getStock", args);
        Thread thread = new Thread(httpConnection);
        //noinspection CallToThreadRun
        thread.run();

        sb.append("Updated stock ").append(stock).append(".");

        return sb.toString();
    }

    public void updateStocksDB(String stock, float price) throws SQLException {
        String queryStockExists = "SELECT name FROM stocks WHERE name = ?;";

        PreparedStatement statement = connection.prepareStatement(queryStockExists);
        statement.setString(1, stock);


        //Put results from query into the resultSet
        ResultSet resultSet = statement.executeQuery();

        //Check if there was any result
        if (resultSet.next()){
            //Update old entry
            String queryStockUpdate = "UPDATE stocks SET `value` = ? WHERE name = ?;";
            PreparedStatement preparedStatement = connection.prepareStatement(queryStockUpdate);
            preparedStatement.setFloat(1, price);
            preparedStatement.setString(2, stock);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            System.out.println("Updated stock in database.");

        } else {
            //Insert new entry
            String queryStockInsert = "INSERT INTO stocks(name, `value`) VALUES (?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(queryStockInsert);
            preparedStatement.setString(1, stock);
            preparedStatement.setFloat(2, price);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            System.out.println("Inserted stock into database.");
        }

        statement.close();


    }

    public String updateCurrencies(String currency){

        StringBuilder sb = new StringBuilder();
        String[] args = new String[1];
        args[0] = currency;

        //Update data of the currency in a new Thread
        HTTPConnection httpConnection = new HTTPConnection(options, "getCurr", args);
        Thread thread = new Thread(httpConnection);
        thread.start();

        sb.append("Trying to update currency now.");

        return sb.toString();
    }

    /**
     * This function updates the database entries which were found in the HTTPConnection
     * @param currency 3-char long currency String
     * @param value float value of the currency in USD.
     * @throws SQLException Throws an SQL Exception.
     */
    public void updateCurrenciesDB(String currency, float value) throws SQLException {
        String sqlQuery = "SELECT currency FROM currencies WHERE currency = ?;";

        PreparedStatement statement = connection.prepareStatement(sqlQuery);
        statement.setString(1, currency);

        //Puts all currencies into resultset.


        ResultSet resultSet = statement.executeQuery();


        if (resultSet.next()) {
            //Update existing currency with new value
            String query = "UPDATE currencies SET `value` = ? WHERE currency = ?";
            PreparedStatement prepState = connection.prepareStatement(query);
            prepState.setFloat(1, value);
            prepState.setString(2, currency);
            prepState.executeUpdate();
            prepState.close();
            System.out.println("Updated currency in database.");
        } else {
            //Need to put in a new entry
            String query = "INSERT INTO currencies(currency, `value`) " +
                    "VALUES (?,?);";
            PreparedStatement prepState = connection.prepareStatement(query);
            prepState.setString(1, currency);
            prepState.setFloat(2, value);
            prepState.executeUpdate();
            prepState.close();
            System.out.println("Created new currency in database.");
        }
        statement.close();

    }

    public void getAllCurrencies() throws SQLException {
        String query = "SELECT * FROM currencies;";
        PreparedStatement prepState = connection.prepareStatement(query);
        ResultSet results = prepState.executeQuery();
        while (results.next()){
            System.out.println(results.getString(1) + ": " +
                    results.getFloat(2));
        }
    }

    public float getSingleStock(String stock) throws SQLException {
        String query = "SELECT * FROM stocks WHERE name = ?;";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, stock);

        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()){
            String name = resultSet.getString("name");
            long id = resultSet.getLong("ID");
            float price = resultSet.getFloat("value");
            System.out.println("name: " + name);
            System.out.println("price: " + price);
            return price;
        }
        preparedStatement.close();
        return 0.0f;

    }
}

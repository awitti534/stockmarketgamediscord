import java.sql.SQLException;

public class DiscordStockHelper {

    /**
     * This function initializes the code to buy stocks.
     * @param userID
     * @param stockName
     * @param amount
     * @return
     */
    public static String initStockBuying(long userID, String stockName, int amount) throws SQLException {

        //Check first if the user is registered.
        if (!DiscordDatabase.isRegistered(userID)) {
            return "User is not registered. You can register with \"!register\"";
        }

        //Need to buy a valid amount of stocks.
        if (amount <= 0) {
            return "Please enter a positive amount of stocks. (Integer only.)";
        }

        //Then update the stock price
        Bank.getInstance().updateStockSynced(stockName);
        float valueOfStock = Bank.getInstance().getSingleStock(stockName);
        //
        if (valueOfStock <= 0){
            return "Stock not found.";
        }

        //Then check if user has enough money
        float totalPrice = valueOfStock * amount;
        float buyerFunds = DiscordDatabase.getFunds(userID);
        int newStockAmount = 0;

        if (totalPrice > buyerFunds){
            //Buyer doesn't have enough money
            return "Failure. Not enough money";
        } else {
            //Buyer has enough money, proceed with buying the stock and removing money.

            int removeFundsStatus = DiscordDatabase.removeFunds(userID, totalPrice);
            if (removeFundsStatus >= 0) {
                //Add stocks to inventory
                newStockAmount = DiscordDatabase.addStockToInvestor(userID, stockName, amount);

            }


        }

        StringBuilder sb = new StringBuilder();
        sb.append("Success. You bought ");
        sb.append(amount);
        sb.append(" of the stock ");
        sb.append(stockName);
        sb.append(" for ");
        sb.append(totalPrice);
        sb.append(" .\n");
        sb.append("You now have ");
        sb.append(newStockAmount);
        sb.append(" in your inventory.\n");

        return sb.toString();
    }

    /**
     * This function prepares the selling of stocks.
     * @param userID long userID
     * @param stockName the name of the stock to be sold
     * @param amount 0 if all, otherwise it sells the specified amount. Using a number too high will sell the
     *               amount which is left in the inventory.
     * @return It returns a status string to be displayed in Discord.
     */
    public static String initStockSelling(long userID, String stockName, int amount) throws SQLException {
        //TODO whole function

        if (!DiscordDatabase.isRegistered(userID)) //check if user is registered
            return "User is not registered. You can register with \"!register\"";

        if (amount < 0) //allow only good amounts of sold stock
            return "Amount specified is too small. Please use a number bigger than 0 (or don't use a number " +
                    "if you want to sell all stock";


        int oldStockAmount = DiscordDatabase.getStockAmount(userID, stockName);
        int toBeSoldAmount;
        if (amount == 0) { //Sell all
            toBeSoldAmount = oldStockAmount;
        } else {
            toBeSoldAmount = Math.min(amount, oldStockAmount); //Sell as much as possible and wanted
        }
        int newStockAmount = oldStockAmount - toBeSoldAmount;

        //Then update the stock price
        Bank.getInstance().updateStocks(stockName);
        float valueOfStock = Bank.getInstance().getSingleStock(stockName);

        if (valueOfStock <= 0){
            return "Stock not found.";
        }

        float totalPrice = toBeSoldAmount * valueOfStock;

        DiscordDatabase.addFunds(userID, totalPrice);
        //Return funds to investor
        DiscordDatabase.updateAmountOfStocks(userID, stockName, newStockAmount);
        //Set new amount of stock in the investors database.

        StringBuilder sb = new StringBuilder();

        sb.append("Successfully sold ");
        sb.append(toBeSoldAmount);
        sb.append(" of stock ");
        sb.append(stockName);
        sb.append(" for ");
        sb.append(totalPrice);
        sb.append(" .\n");

        return sb.toString();
    }

    public static String initShowInventory(long userID) throws SQLException {
        StringBuilder sb = new StringBuilder();

        //Show money
        float funds = DiscordDatabase.getFunds(userID);

        sb.append("Your money: ");
        //replace with own currency later
        sb.append(funds);
        sb.append("$.\n\n");

        //show inventory
        sb.append(DiscordDatabase.getInventory(userID));


        return sb.toString();
    }
}

import java.sql.SQLException;
import java.util.LinkedList;

public class ConsoleWorker implements Runnable {
    enum currencies {EUR, USD}
    static private long startTime;
    final static private int THREE_HUNDRED_MS = 300;
    static private LinkedList<String> commandList;
    static private Bank bank;

    public static void setOptions(Options options) {
        ConsoleWorker.options = options;
    }

    static private Options options;

    /**
     * Constructor, creates an empty LinkedList for all commands and gets
     * the instance of the bank.
     */
    public ConsoleWorker(){
        commandList = new LinkedList<>();
        bank = Bank.getInstance();
    }


    @Override
    public void run() {
        startTime = System.currentTimeMillis();

        //Endless loop
        while (true){

            //Sleep while there are no commands to be executed
            while (commandList.size() == 0) {
                try {
                    Thread.sleep(THREE_HUNDRED_MS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
             }
            String nextCommand = commandList.getFirst();
            commandList.removeFirst();

            if (nextCommand.equals("exit")){
                String[] exitArray = {"exit"};
                DiscordMain.addCommand(exitArray);
                Main.setStatus(-1);
                //Close the program
                break;
            } else {
                //interpret next command

                //Create array for all the parts of a command with its attributes
                String[] commandParts = nextCommand.split("\\s");

                switch (commandParts[0].toLowerCase()){
                    case "?" :
                    case "help" :
                        showCommands(commandParts);
                        break;
                    case "currencies" :
                        try {
                            showCurrencies();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        break;
                    case "uptime":
                        System.out.println(showUptime());
                        break;
                    case "update":
                        System.out.println(updateDatabase(commandParts));
                        break;
                    case "show":
                    case "display":
                        try {
                            showData(commandParts);
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        break;
                    case "setup":
                        try {
                            bank.firstTimeSetup();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        break;
                    case "discord":
                        discordCommand(commandParts);
                        break;
                    default:
                        System.out.println("Command not recognized. Write help for a list of commands.");

                }

            }
        }
        Main.setStatus(5);
    }

    /**
     * Add a command to be executed.
     * @param command as one line. Arguments are included and separated by spaces.
     */
    static public void addCommand(String command){
        commandList.addLast(command);
    }

    public static String showUptime(){
        //LocalDate currentTime = LocalDate.now();
        long currentTime = System.currentTimeMillis();
        long totalSeconds = (currentTime - startTime) / 1000; //ms to s
        //TODO Implement Days, Hours and minutes.
        final int secondsPerMinute = 60;
        long totalMinutes = totalSeconds / secondsPerMinute;

        final int minutesPerHour = 60;
        long totalHours = totalMinutes / minutesPerHour;

        final int hoursPerDay = 24;
        long totalDays = totalHours / hoursPerDay;

        long remainingHours = totalDays % hoursPerDay;
        long remainingMinutes = totalMinutes % minutesPerHour;
        long remainingSeconds = totalSeconds % secondsPerMinute;

        StringBuilder sb = new StringBuilder();
        sb.append("The bot has been running for ");
        if (totalDays > 0) {
            sb.append(totalDays);
            sb.append(" days, ");
        }
        if (remainingHours > 0) {
            sb.append(remainingHours);
            sb.append(" hours, ");
        }
        if (remainingMinutes > 0) {
            sb.append(remainingMinutes);
            sb.append(" minutes, ");
        }
        sb.append(remainingSeconds);
        sb.append(" seconds.");

        return sb.toString();
    }

    public static String updateDatabase(String[] inputArray){
        StringBuilder sb = new StringBuilder();

        //Check for amount of arguments
        if (inputArray.length < 2){
            sb.append("Not enough arguments. Please add entity to be updated.");
        } else {

            //Repeat loop for every single argument
            for (int i = 1; i < inputArray.length; i++){
                String cArg = inputArray[i].toLowerCase(); //current argument

                //Check if currency
                if (cArg.equals("eur") || cArg.equals("usd") || cArg.equals("jpy")){
                    sb.append("Trying to update currency.");
                    sb.append(bank.updateCurrencies(cArg));
                } else {
                    sb.append(bank.updateStocks(cArg));
                }
            }

        }
        return sb.toString();
        
    }

    private void showData(String[] inputArray) throws SQLException {
        //TODO implement showData
        //Go through all arguments
        for (int i = 1; i < inputArray.length; i++) {
            bank.getSingleStock(inputArray[i]); //
        }
    }

    private void showCommands(String[] commandparts) {
        //TODO show all commands
        StringBuilder sb = new StringBuilder();
        if (commandparts.length == 1) {
            sb.append("help - Show all commands.\n");
            sb.append("setup - Performs the first time setup for the database.\n");
            sb.append("uptime - Show the amount of time the programm has been running.\n");
            sb.append("update - Gets fresh data from specified input.\n");
            sb.append("show - Shows the values of certain stocks.\n");
            sb.append("currencies - Shows the values of all current currencies.\n");
            sb.append("authdiscord - Authenticate the client for the Discord service.\n");
            sb.append("exit - Closes the program.");
        } else if (commandparts.length == 2) {
            switch (commandparts[1]){
                case "help" :
                    sb.append("Write help command to see how a certain command works.");
                    break;
                case "setup" :
                    sb.append("Run it the first time the program is used. It clears the existing database " +
                            "and sets up the tables for this program. All entries will be lost. Use with caution.");
                    break;
                case "uptime" :
                    sb.append("Simple command to show how long the program has been running.");
                    break;
                case "update" :
                    sb.append("Usage: Write all the stocks and currencies you want to update over the internet. " +
                            "You can write multiple entities, being separated by a space. Currencies are three " +
                            "letters long.");
                    break;
                case "show" :
                    sb.append("Shows the current saved value of a stock. It does not pull live data from the internet.");
                    break;
                case "currencies" :
                    sb.append("Shows all current saved currencies. Any argument is ignored.");
                    break;
                case "exit" :
                    sb.append("Closes all connections and shuts down the program.");
                    break;
                default:
                    sb.append("Not recognized command. Write help for all commands.");
                    break;
            }
        } else {
            sb.append("Too many arguments.");
        }
        System.out.println(sb.toString());
    }

    /**
     * Calls function to display all currencies with their current values.
     * @throws SQLException Throws an SQL Exception.
     */
    private void showCurrencies() throws SQLException {
        bank.getAllCurrencies();
    }

    private void discordCommand(String[] command){
        DiscordMain discordMain = DiscordMain.getInstance();
        DiscordMain.addCommand(command);

    }
}

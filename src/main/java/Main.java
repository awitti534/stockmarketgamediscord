import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This is the main class for this project. It's responsible for starting various components. In one thread there will
 * be a connection to the Discord servers, in another thread there will be done HTTP requests.
 */
public class Main {

    static Options options;
    static Bank bank;
    static DiscordMain disCon;
    static ConsoleWorker consoleWorker;
    private static int status;

    static final int THREE_SECONDS = 3000;

    /**
     * Main class for running the program.
     * @param args Not used yet.
     */
    public static void main(String[] args) throws IOException, InterruptedException {

        //Load the options into the program
        final String optionsFilePath = "options.txt";
        File optionsFile = new File(optionsFilePath);
        options = new Options(optionsFile);

        //Start the bank with its database connection
        Bank bank = Bank.getInstance();
        bank.initDBConnection();
        Bank.setOptions(options);

        //Initialize admins list
        RoleStuff.initialize();

        //Start the executer thread
        consoleWorker = new ConsoleWorker();
        Thread threadExec = new Thread(consoleWorker);
        ConsoleWorker.setOptions(options);
        threadExec.start();

        //Start the Discord connection
        DiscordMain discordMain = new DiscordMain();
        DiscordMain.setOptions(options);
        Thread threadDisc = new Thread(discordMain);
        threadDisc.start();

        //Reader for console inputs.
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Waiting for input. Write help for help.");

        status = 0;

        //Endless loop until console input "exit". Sends commands to executer.
        while (status == 0) {
            String input;
            input = br.readLine();
            if(input.contains("exit"))
                status = 5;
            ConsoleWorker.addCommand(input);
        }

        //Wait three seconds for all threads to stop.
        threadExec.join(THREE_SECONDS);
        threadDisc.join(THREE_SECONDS);
        System.out.println("Exiting program. Goodbye :)");

    }

    public static void setStatus(int status){
        Main.status = status;

    }


}

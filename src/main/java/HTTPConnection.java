import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.SQLException;
import java.time.Duration;

/**
 * This is a class for creating HTTP Connections to a server to get some data. It needs to get data from an Options
 * class.
 */
public class HTTPConnection implements Runnable {

    final static long FIVE_SECONDS = 5;

    private static String APIKeyWebsite;
    private static String command;
    private static String[] args;
    private static Bank bank;


    /**
     * Constructor for a HTTPConnection Thread.
     * @param options Give a static options class which has the API keys.
     * @param command The command as a String which has to be executed.
     * @param args Optional arguments for the command.
     */
    public HTTPConnection(Options options, String command, String[] args){
        APIKeyWebsite = options.getAPIWebsite();
        HTTPConnection.command = command;
        HTTPConnection.args = args;
        bank = Bank.getInstance();
    }

    public void run() {

        //Read command
        switch (command) {
            case "getCurr":
                try {
                    getCurrUpdate(args[0].toUpperCase());
                } catch (IOException | InterruptedException | SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "getStock":
                try {
                    getStockUpdate(args[0].toLowerCase());
                } catch (IOException | InterruptedException | SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("No recognized command for the HTTP connection thread.");
        }

    }


    /**
     * In this function we get new data for currencies.
     * @param currency as a 3-letter long string
     */
    private void getCurrUpdate(String currency) throws IOException, InterruptedException, SQLException {

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&" +
                        "to_currency=" + currency + "&apikey=" + APIKeyWebsite))
                .timeout(Duration.ofSeconds(FIVE_SECONDS))
                .GET()
                .build();

        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        int statusCode = httpResponse.statusCode();

        switch (statusCode){
            case 200:
                JSONObject joResponse = new JSONObject(httpResponse.body());
                JSONObject joField = joResponse.getJSONObject("Realtime Currency Exchange Rate");
                String exchangeRate = joField.getString("5. Exchange Rate");
                bank.updateCurrenciesDB(currency, Float.parseFloat(exchangeRate));
                System.out.println("Updated currency.");
                break;
            case 401:
                System.out.println("Error 401. Not authorization.");
                break;
            case 404:
                System.out.println("Error 404. Recourse not found.");
                break;
            default:
                System.out.println("Error with HttpRequest. Status code: " + statusCode);
        }
    }

    private void getStockUpdate(String stock) throws IOException, InterruptedException, SQLException {
        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest stockRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://www.alphavantage.co/query?function=" +
                        "GLOBAL_QUOTE&symbol=" + stock + "&apikey=" + APIKeyWebsite))
                .timeout(Duration.ofSeconds(FIVE_SECONDS))
                .GET()
                .build();

        HttpResponse<String> stockResponse = httpClient.send(stockRequest, HttpResponse.BodyHandlers.ofString());

        int statusCode = stockResponse.statusCode();

        switch (statusCode){
            case 200:
                JSONObject jStockResponse = new JSONObject(stockResponse.body());
                JSONObject joField = jStockResponse.getJSONObject("Global Quote");

                //See if there is any result
                String testCost = joField.optString("05. price");
                if (testCost == null || testCost.equals("")) {
                    System.out.println("Error: No stock found with this name.");
                    break;
                }

                String costString = joField.getString("05. price");
                System.out.println("Cost : " + costString);
                bank.updateStocksDB(stock, Float.parseFloat(costString));
                break;
            case 401:
                System.out.println("Error 401. Not authorization.");
                break;
            case 404:
                System.out.println("Error 404. Recourse not found.");
                break;
            default:
                System.out.println("Error with HttpRequest. Status code: " + statusCode);
        }
    }
}

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.user.User;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;


public class DiscordMain implements Runnable {

    private static Options options;
    private static final DiscordMain DISCORD_MAIN = new DiscordMain();
    private static final LinkedList<String[]> commandList = new LinkedList<>();
    private static final int THREE_HUNDRED_MS = 300;
    private static Set<String> adminList;
    private static DiscordApi api;
    //TODO Main channel

    public DiscordMain(){
    }

    public static void setOptions(Options options){
        DiscordMain.options = options;
    }

    public static DiscordMain getInstance(){
        return DISCORD_MAIN;
    }

    private static DiscordApi getApi() {
        return api;
    }


    /**
     * This is the main function of the Discord connection, which runs all the time.
     */
    public void run() {
        /*
        assert client != null;
        showLoginCreds(client);
        startInputListener(client);
        //testFunction(client);
        */

        //Build the access for all Discord features
        api = new DiscordApiBuilder().setToken(options.getAPIDiscordToken()).login().join();
        startListener(api);

        //Get admin List
        adminList = RoleStuff.getAdminSet();


        while (true){
            if (commandList.size() == 0){
                try {
                    Thread.sleep(THREE_HUNDRED_MS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {

                //Get latest command
                StringBuilder output = new StringBuilder();
                String[] input = commandList.pop();
                String command = input[0];

                if (command.equals("exit")){
                    api.disconnect();
                    break;
                }
                switch (command){
                    default :
                        output.append("Not recognized command.");
                }

                System.out.println(output.toString());
            }
        }
    }

    public static void addCommand(String[] command){
        commandList.addLast(command);
    }

    /**
     * This is the main listener, which interprets all incoming messages.
     * @param api A DiscordApi object which is already logged in.
     */
    private void startListener(DiscordApi api){
        api.addMessageCreateListener(event -> {
            String input = event.getMessageContent();
            long authorId = event.getMessageAuthor().getId();



            //Only interpret commands starting with !
            if (input.startsWith("!") && authorId != api.getClientId()){
                String[] inputParts = input.split("\\s"); //Split at whitespace
                StringBuilder sb = new StringBuilder();

                try {
                    sb.append(interpretCommand(inputParts, authorId));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                System.out.println(sb.toString());
                event.getChannel().sendMessage(sb.toString());

            }

        });
    }

    private String helpCommand(){
        StringBuilder sb = new StringBuilder();
        sb.append("!register - Register to use the stock exchange game.\n");
        sb.append("!inventory - Show your inventory of stocks with its values.\n");
        sb.append("!update - Update a stock or currency of your choice.\n");
        sb.append("!buy - Buy a stock.\n");
        sb.append("!sell - Sell a stock.\n");
        sb.append("!currency - Choose the currency you want your inventory to be displayed in.\n");
        sb.append("!help - Display help. With argument it gives more help on certain command.\n");

        return sb.toString();
    }

    private String helpCommand(String command){
        StringBuilder sb = new StringBuilder();

        return sb.toString();
    }

    private boolean isRegistered(long userId) throws SQLException {
        return DiscordDatabase.isRegistered(userId);
    }

    private boolean isAdmin(long userId){
        return adminList.contains(String.valueOf(userId));
    }

    private boolean isBot(long userId){
        //TODO check if is bot
        return false;
    }

    private String interpretCommand(String[] args, long userId) throws SQLException {
        StringBuilder returny = new StringBuilder();
        //TODO interpret command

        switch (args[0]) {
            case "!help":
                if (args.length == 1) {
                    //General help
                    returny.append(helpCommand());
                } else if (args.length == 2) {
                    //Specific help
                    returny.append(helpCommand(args[1]));
                } else {
                    returny.append("Wrong amount of arguments.");
                }
                break;
            case "!register" :
                if (!isBot(userId)){
                    if (!isRegistered(userId)){

                        //register user
                        if (DiscordDatabase.registerUser(userId)){
                            //Registered successfully
                            returny.append("Successfully registered ");
                            returny.append(userId);
                            returny.append(".");
                        } else {
                            //Register not successfully
                            returny.append("Couldn't register ");
                            returny.append(userId);
                            returny.append(".");
                        }
                    } else {

                        //user already registered
                        returny.append("User ");
                        returny.append(userId);
                        returny.append(" is already registered.");
                    }
                }
                break;
            case "!exit" :
                if (isAdmin(userId)) {
                    returny.append("Shutting down the bot.");
                    String[] exitCommand = {"exit"};
                    addCommand(exitCommand);
                    ConsoleWorker.addCommand("exit");
                    Main.setStatus(-1);
                    System.out.println("Discord disconnected.");
                }
                break;
            case "!update" :
                //Update stocks/currencies.
                returny.append(ConsoleWorker.updateDatabase(args));
                break;
            case "!uptime" :
                returny.append(ConsoleWorker.showUptime());
                break;

            case "!buy" :
                if (args.length != 3){
                    returny.append("Please use the command the following way: !buy stock amount");
                } else {
                    String stockName = args[1];
                    int stockAmount = Integer.parseInt(args[2]);
                    returny.append(DiscordStockHelper.initStockBuying(userId, stockName, stockAmount));
                }
                break;

            case "!funds" :
                returny.append(DiscordDatabase.getFunds(userId));
                break;

            case "!sell" :
                if (args.length < 2 || args.length > 3)
                    returny.append("Please use the command the following way: !sell stock [amount]");
                else {
                    String stockName = args[1];
                    if (args.length == 2) {
                        //sell all stocks
                        returny.append(DiscordStockHelper.initStockSelling(userId, stockName, 0));
                    } else {
                        //sell specified amount of stocks
                        int amount = Integer.parseInt(args[2]);
                        if (amount == 0)
                            amount --; //hack so amount is not valid
                        returny.append(DiscordStockHelper.initStockSelling(userId, stockName, amount));
                    }
                }
                break;

            case "!inventory" :
                returny.append(DiscordStockHelper.initShowInventory(userId));
                break;

            default:
                //Output if command is not recognized
                returny.append("Command not recognized.");

        }
        return returny.toString();

    }

}

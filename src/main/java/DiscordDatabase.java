import java.sql.*;

public class DiscordDatabase {
    final static int USER_NOT_REGISTERED = -1;
    public final static int NO_STOCK_IN_INVENTORY = -4;
    static Connection connection = Bank.getConnection();

    public static boolean isRegistered(long userID) throws SQLException {

        final String sqlString = "SELECT name FROM  investors WHERE " +
                "name = ?;";
        PreparedStatement statement = connection.prepareStatement(sqlString);
        statement.setLong(1, userID);

        ResultSet rs = statement.executeQuery();

        //Return if there is one result.
        return rs.next();

    }

    /**
     * It registers a user in the investor table in the Database.
     * @param userID long Discord ID of the investor to be registered
     * @return True if success, False if already registered
     * @throws SQLException Throw it if there is a problem with the SQL statement.
     */
    public static boolean registerUser(long userID) throws SQLException {

        if (isRegistered(userID)) {
            //User is already registered

            return false;
        } else {
            //Register the user.

            final String sqlString = String.format("INSERT INTO investors (name, currency, funds) " +
                    " VALUES (%s, \"eur\", 15000) ", userID);

            PreparedStatement statement = connection.prepareStatement(sqlString);
            statement.executeUpdate();
            return true;
        }

    }

    /**
     * This function returns the current funds of the user which are specified in the database.
     * @param userID long value
     * @return The funds as a float. If invalid, it returns -1.
     */
    public static float getFunds(long userID) throws SQLException {

        if (!isRegistered(userID)) { //return -1 if user isn't registered
            return -1;
        } else {
            //get the funds from the database
            final String sqlString = "SELECT funds from investors WHERE " +
                    "name = ?;";
            PreparedStatement statement = connection.prepareStatement(sqlString);
            statement.setLong(1, userID);

            ResultSet rs = statement.executeQuery();

            return rs.getFloat(1);

        }

    }


    /**
     * This function adds funds to a user in the database. The amount mustn't be
     * negative.
     * @param userID userID as a long variable.
     * @param amount How many funds will be added to the amount.
     * @return Negative int if error, positive if success.
     * @throws SQLException if there is a problem with the SQL update
     */
    public static int addFunds(long userID, float amount) throws SQLException {
        if (!isRegistered(userID)) { //return -1 if user isn't registered
            return -1;
        } else {
            if (amount < 0) { //return -2 if amount is negative
                return -2;
            } else {

                float currentFunds = getFunds(userID);
                float newBalance = currentFunds + amount;

                final String sqlString = "UPDATE Investors " +
                        "SET funds = ? " +
                        "WHERE name = ?;";

                PreparedStatement statement = connection.prepareStatement(sqlString);
                statement.setFloat(1, newBalance);
                statement.setLong(2, userID);

                int result = statement.executeUpdate();
                System.out.println("addFundsTest: " + result);

                return result;


            }
        }
    }

    /**
     * This function removes funds from a user in the database. The amount mustn't be
     * negative.
     * @param userID userID as a long variable.
     * @param amount How many funds will be removed from the account.
     * @return Negative int if error, positive if success.
     * @throws SQLException if there is a problem with the SQL update
     */
    public static int removeFunds(long userID, float amount) throws SQLException {
        if (!isRegistered(userID)){ //return -1 if user isn't registered
            return -1;
        } else {
            if (amount < 0) { //return -2 if amount is negative
                return -2;
            } else {

                float currentFunds = getFunds(userID);
                float newBalance = currentFunds - amount;

                if (newBalance < 0) { //return -3 if new balance would be too small
                    return  -3;
                } else {
                    final String sqlString = "UPDATE Investors " +
                            "SET funds = ? " +
                            "WHERE name = ?;";

                    PreparedStatement statement = connection.prepareStatement(sqlString);
                    statement.setFloat(1, newBalance);
                    statement.setLong(2, userID);

                    int result = statement.executeUpdate();
                    System.out.println("removeFundsTest: " + result);

                    return result;
                }
            }
        }
    }

    /**
     * This function handles adding stocks to the inventory of an investor
     * @param userID long userID
     * @param stockName the name of the stock as a string
     * @param amount The amount of stocks to be added
     * @return The amount of this particular stock the investor will have in the inventory.
     * @throws SQLException If there is a problem with the SQL query.
     */
    public static int addStockToInvestor(long userID, String stockName, int amount) throws SQLException {
        if (!isRegistered(userID)){ //return -1 if user isn't registered
            return USER_NOT_REGISTERED;
        } else {

            int oldStockAmount = getStockAmount(userID, stockName);
            int statusAddStocks;

            if (oldStockAmount == NO_STOCK_IN_INVENTORY){
                statusAddStocks = addNewStockToInventory(userID, stockName, amount);
                return amount;
            } else {
                int newStockAmount = oldStockAmount + amount;
                statusAddStocks = updateAmountOfStocks(userID, stockName, newStockAmount);
                return newStockAmount;

            }
        }
    }


    /**
     * This function gets the amount of stocks a person has to a specified stock.
     * @param userID userID as long value.
     * @param stockName the name of the stock you want to know the amount of.
     * @return int value. -4 means no stock available. -1 means user is not registered. Values >=
     * 0 are the actual amount.
     * @throws SQLException If there is a problem with the SQL statement.
     */
    public static int getStockAmount(long userID, String stockName) throws SQLException {
        if (!isRegistered(userID))
            return USER_NOT_REGISTERED;

        String sqlStatement = "SELECT amount FROM inventory WHERE " +
                "stockname = ? AND investor = ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);

        preparedStatement.setString(1, stockName);
        preparedStatement.setLong(2, userID);

        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            return resultSet.getInt(1);
        }

        return NO_STOCK_IN_INVENTORY;
    }

    /**
     * This function adds a new stock entry to the inventory database. It's only used if there isn't already
     * an entry.
     * @param userID long userID
     * @param stockName name of the Stock
     * @param amount amount of the stock the user will have.
     * @return Status codes as integer.
     * @throws SQLException If there is a problem with the SQL statement.
     */
    private static int addNewStockToInventory(long userID, String stockName, int amount) throws SQLException {

        if (!isRegistered(userID))
            return USER_NOT_REGISTERED;

        String sqlStatement = "INSERT INTO inventory(stockname, investor, amount) " +
                "VALUES (?,?,?);";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);

        preparedStatement.setString(1, stockName);
        preparedStatement.setLong(2, userID);
        preparedStatement.setInt(3, amount);

        boolean status = preparedStatement.execute();



        return status? 1 : -5;
    }

    public static int updateAmountOfStocks(long userID, String stockName, int newStockAmount) throws SQLException {
        
        if (!isRegistered(userID))
            return USER_NOT_REGISTERED;

        String sqlStatement = "UPDATE inventory SET amount = ? WHERE stockname = ? AND investor = ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);

        preparedStatement.setInt(1, newStockAmount);
        preparedStatement.setString(2, stockName);
        preparedStatement.setLong(3, userID);

        boolean status = preparedStatement.execute();

        return status? 1 : -5;
    }

    public static String getInventory(long userID) throws SQLException {
        StringBuilder sb = new StringBuilder();
        if (!isRegistered(userID))
            return "User is not registered.";

        //SQL query to get all stocks from one investor
        String sqlQuery = "SELECT stockname, amount FROM inventory WHERE investor = ?";
        PreparedStatement prepState = connection.prepareStatement(sqlQuery);
        prepState.setLong(1, userID);

        ResultSet rs = prepState.executeQuery();

        while (rs.next()) { //get all results from the query
            String stockName = rs.getString(1);
            int stockAmount = rs.getInt(2);
            sb.append(stockAmount);
            sb.append("x   ");
            sb.append(stockName);
            sb.append("\n");
        }

        return sb.toString();
    }

}

import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;

/**
 * This is a class for loading options from an options file. In these options there are API keys.
 */
public class Options {

    private String APIAlphavantage;
    private String APIDiscordClientID;
    private String APIDiscordSecret;
    private String APIDiscordToken;

    //These are the values for the default options file.
    final String jsonAPIAlphavantageKey = "AlphavantageAPI";
    final String mockAPIAlphavantageValue = "REPLACE WITH API KEY";
    final String jsonAPIDiscordClientKey = "DiscordClientID";
    final String mockAPIDiscordClientValue = "REPLACE WITH DISCORD CLIENT ID";
    final String jsonAPIDiscordSecretKey = "DiscordSecret";
    final String mockAPIDiscordSecretValue = "REPLACE WITH DISCORD SECRET";
    final String jsonAPIDiscordTokenKey = "DiscordToken";
    final String mockAPIDiscordTokenValue = "REPLACE WITH BOT TOKEN";


    public Options(File optionsFile) throws IOException {

        //Create a new options file if none exists, else load all existing options
        if (optionsFile.createNewFile()){

            JSONObject jobject = new JSONObject();
            jobject.put(jsonAPIAlphavantageKey, mockAPIAlphavantageValue);
            jobject.put(jsonAPIDiscordClientKey, mockAPIDiscordClientValue);
            jobject.put(jsonAPIDiscordSecretKey, mockAPIDiscordSecretValue);
            jobject.put(jsonAPIDiscordTokenKey, mockAPIDiscordTokenValue);
            String jsonString = jobject.toString();

            //Writer for writing into an options file.
            BufferedWriter writy  = new BufferedWriter(new FileWriter(optionsFile));

            //Writing into the options file.
            writy.write(jsonString);
            writy.close();

            System.out.println("options.txt created. Please fill in API keys and restart the program.");


        } else {
            //Load options file
            ArrayList<String> results = new ArrayList<>();
            ArrayList<JSONObject> options = new ArrayList<>();

            FileReader fr = new FileReader(optionsFile);
            BufferedReader br = new BufferedReader(fr);

            String line;

            //Read lines from options.txt and add to results String-Array
            while((line = br.readLine()) != null) {
                results.add(line);
            }


            //Read all result strings and create a JSONObject for each
            for (String s : results) {
                JSONObject job = new JSONObject(s);
                System.out.println(job.toString());
                options.add(job);
            }

            //Move API Keys into class variables.
            for (JSONObject jso : options){
                String tempAlphaKey = jso.optString(jsonAPIAlphavantageKey);
                String tempDiscordClientID = jso.optString(jsonAPIDiscordClientKey);
                String tempDiscordSecret = jso.optString(jsonAPIDiscordSecretKey);
                String tempDiscordToken = jso.optString(jsonAPIDiscordTokenKey);

                if (!tempAlphaKey.equals(mockAPIAlphavantageValue)){
                    APIAlphavantage = tempAlphaKey;
                }

                if (!(tempDiscordClientID.isEmpty()
                        ||tempDiscordClientID.equals(mockAPIDiscordClientValue))){
                    APIDiscordClientID = tempDiscordClientID;
                    //System.out.println("DEBUG: Discord client ID read.");
                }

                if (!tempDiscordSecret.equals(mockAPIDiscordSecretValue)){
                    APIDiscordSecret = tempDiscordSecret;
                    //System.out.println("DEBUG: Discord secret read.");
                }

                if (!(tempDiscordToken.isEmpty() ||
                        tempDiscordToken.equals(mockAPIDiscordTokenValue))){
                    APIDiscordToken = tempDiscordToken;
                }
            }

            br.close(); //Close the file access.

        }

    }

    public String getAPIWebsite(){
        return APIAlphavantage;
    }

    public String getAPIDiscordClientID(){
        return APIDiscordClientID;
    }

    public String getAPIDiscordSecret() {
        return APIDiscordSecret;
    }

    public String getAPIDiscordToken() {
        return APIDiscordToken;
    }
}
